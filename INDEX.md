# Vi IMproved (vim)

Improved version of the "vi" editor, one of the standard text editors on UNIX systems. These are the runtime files for VIM. [GPL-compatible, but asks that if you find it useful you make a donation to help children in Uganda through the ICCF. The full license text can be found in README.txt.] DOS 32-bit only (since 7.1).

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## VIM.LSM

<table>
<tr><td>title</td><td>Vi IMproved (vim)</td></tr>
<tr><td>version</td><td>7.3a</td></tr>
<tr><td>entered&nbsp;date</td><td>2017-05-05</td></tr>
<tr><td>description</td><td>Improved version of the "vi" editor</td></tr>
<tr><td>summary</td><td>Improved version of the "vi" editor, one of the standard text editors on UNIX systems. These are the runtime files for VIM. [GPL-compatible, but asks that if you find it useful you make a donation to help children in Uganda through the ICCF. The full license text can be found in README.txt.] DOS 32-bit only (since 7.1).</td></tr>
<tr><td>keywords</td><td>editor, vi, vim</td></tr>
<tr><td>author</td><td>Bram Moolenaar &lt;Bram -at- Moolenaar.net&gt;</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.vim.org/</td></tr>
<tr><td>platforms</td><td>*nix, Windows, Macintosh, DOS, others</td></tr>
<tr><td>copying&nbsp;policy</td><td>Source code available (open)</td></tr>
</table>
